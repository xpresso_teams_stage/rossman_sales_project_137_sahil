__all__ = ["StatusReporter"]
__author__ = "Srijan Sharma"

import os

from xpresso.ai.core.commons.utils.generic_utils import str2bool
from xpresso.ai.core.commons.utils.singleton import Singleton
from xpresso.ai.core.commons.utils.constants import ENABLE_LOCAL_EXECUTION
from xpresso.ai.core.data.pipeline.pipeline_component_controller import \
    PipelineController


class StatusReporter(metaclass=Singleton):
    def __init__(self, component_name=None, xpresso_run_name=None):
        self.name = component_name
        self.xpresso_run_name = xpresso_run_name
        return

    def report_pipeline_status(self, status):
        print("Parent component reporting status")
        if str2bool(os.environ.get(ENABLE_LOCAL_EXECUTION, "False")):
            print("Reporting status: {}".format(str(status)))
        else:
            print("Reporting status: {}".format(str(status)))
            PipelineController().report_pipeline_status(self.xpresso_run_name,
                                                        self.name, status)
