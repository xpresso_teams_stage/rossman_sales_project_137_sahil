__all__ = ["ExploreNumeric"]
__author__ = ["Srijan Sharma"]

from xpresso.ai.core.data.automl.dataset_type import DECIMAL_PRECISION
import pandas as pd
import numpy as np
from decimal import Decimal
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.constants import \
    DEFAULT_OUTLIER_MARGIN_NUMERIC, \
    DEFAULT_PROBABILITY_BINS, \
    DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class ExploreNumeric():

    def __init__(self, data, threshold=DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD,
                 outlier_margin=DEFAULT_OUTLIER_MARGIN_NUMERIC,
                 probability_dist_bins=DEFAULT_PROBABILITY_BINS):
        self.data = data
        self.outlier_margin = outlier_margin
        self.probability_dist_bins = probability_dist_bins
        if threshold is None:
            self.threshold = DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD
        else:
            self.threshold = threshold
        return

    def populate_numeric(self):
        """Sets the metrics for numeric attribute
            Args:
            Return:
                """
        resp = dict()
        min, max, mean, median, std, var, mode, quartiles, deciles, \
        outliers, pdf, iqr, kurtosis, skewness, is_valid = self.numeric_analysis(
            self.data, self.threshold,
            self.outlier_margin, self.probability_dist_bins)

        resp["min"] = min
        resp["max"] = max
        resp["mean"] = mean
        resp["median"] = median
        resp["std"] = std
        resp["var"] = var
        resp["mode"] = mode
        resp["quartiles"] = quartiles
        resp["deciles"] = deciles
        resp["outliers"] = outliers
        resp["pdf"] = pdf
        resp["iqr"] = iqr
        resp["kurtosis"] = kurtosis
        resp["skewness"] = skewness
        resp["is_valid"] = is_valid
        return resp

    @staticmethod
    def numeric_analysis(data, threshold,
                         outlier_margin=DEFAULT_OUTLIER_MARGIN_NUMERIC,
                         probability_dist_bins=DEFAULT_PROBABILITY_BINS):
        """Performs analysis of numeric attribute
        Args:
        Returns:
                min (:int): Minimum numeric value
                max (:int): Maximum numeric value
                mean (:int): Mean of the values of attribute
                median (:int): Median of the values of attribute
                std (:int): Standard Deviation of the values of attribute
                var (:int): Variance of the values of attribute
                mode (:int): Mode of the values of attribute
                quartiles (:list): Quartiles of attribute
                deciles (:list): Deciles of attribute
               outliers (:list): Outliers of the attribute
               pdf (:dict): Probability distribution of the values of attribute
               iqr (:list): Inter quartile range
               kurtosis (:int): Kurtosis of the values of attribute
               is_valid(:bool): Boolean indicating validity of attribute
           """
        summary = data.describe().round(DECIMAL_PRECISION)

        min = summary["min"]
        max = summary["max"]
        mean = summary["mean"]
        std = summary["std"]
        var = std * std
        mode = data.mode().round(DECIMAL_PRECISION)
        if len(mode) >= 1:
            mode = mode[0]
        else:
            mode = "na"
            logger.info("Unable to find mode for {}.".format(data.name))
            # For mode na
            print("Unable to find mode for {}.".format(data.name))

        quartiles = data.quantile([.0, .25, .5, .75, 1]).round(
            DECIMAL_PRECISION).values

        if len(quartiles) == 5:
            median = quartiles[2]
            iqr = quartiles[3] - quartiles[1]
        else:
            iqr = "na"
            median = "na"
            logger.info(
                "Insufficient number of quartiles, Unable to find inter "
                "quartile range and median for {}.".format(data.name))
            # For insufficient data information
            print(
                "Insufficient number of quartiles, Unable to find inter "
                "quartile range and median for {}.".format(data.name))
        try:
            deciles = ExploreNumeric.calculate_deciles(np.array(data), 10)
        except ValueError as e:
            try:
                logger.info("Dropping duplicates for calculating deciles for {}"
                            .format(data.name))
                # For dropping duplicates from data
                print("Dropping duplicates for calculating deciles for {}"
                      .format(data.name))
                deciles = ExploreNumeric.calculate_deciles(np.array(data), 10,
                                                           duplicates='drop')
            except (IndexError,KeyError) as e:
                # When only one numeric value in the attribute
                deciles = ExploreNumeric.calculate_deciles(np.array(data), len(
                    data.dropna().unique()), duplicates='drop')
        outliers = round(data[(np.abs(data - mean) > (
                outlier_margin * std))],
                         DECIMAL_PRECISION).to_numpy()

        pdf = pd.cut(data, probability_dist_bins).value_counts()
        pdf = pdf.sort_index()
        new_index = list()
        val_list = list()
        for ind, val in pdf.items():
            new_index.append("({:.2e}, {:.2e}]".format(Decimal(ind.left),
                                                       Decimal(ind.right)))
            val_list.append(val)
        pdf = pd.Series(val_list, index=new_index)
        pdf.index = pdf.index.astype(str)
        pdf = pdf.to_dict()
        kurtosis = round(data.kurtosis(), DECIMAL_PRECISION)
        skewness = round(data.skew(axis=0, skipna=True), DECIMAL_PRECISION)

        is_numeric_count = pd.to_numeric(pd.Series(data), errors='coerce') \
            .notna().value_counts()
        total_data = data.size
        try:
            numeric_percent = round(
                float(is_numeric_count[1] / total_data) * 100
                , DECIMAL_PRECISION)
        except KeyError and IndexError:
            numeric_percent = round(float(is_numeric_count / total_data) * 100
                                    , DECIMAL_PRECISION)
        if numeric_percent >= threshold:
            is_valid = True
        else:
            is_valid = False

        return min, max, mean, median, std, var, mode, quartiles, deciles, \
               outliers, pdf, iqr, kurtosis, skewness, is_valid

    @staticmethod
    def calculate_deciles(data, quantiles=10, duplicates='raise'):
        """
        Calculates deciles using pandas qcut
        Args:
            data(obj:Dataframe): The data of which deciles to be calculated
            quantiles(int): Number of quantiles to cut dataframe into.
            duplicates : {default ‘raise’, ‘drop’}, optional
                If bin edges are not unique, raise ValueError or drop
                non-uniques.
        """
        try:
            deciles = \
                pd.qcut(data, quantiles, duplicates=duplicates, retbins=True)[
                    1].round(DECIMAL_PRECISION)
        except (ValueError, IndexError, KeyError) as e:
            raise e
        return deciles
