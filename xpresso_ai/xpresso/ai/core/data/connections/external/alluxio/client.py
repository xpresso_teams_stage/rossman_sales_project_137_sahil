"""Class design for Alluxio connectivity"""

import json
import os
import telnetlib
from io import BytesIO

import alluxio
import alluxio.exceptions
import alluxio.wire
import pandas as pd

import xpresso.ai.core.commons.utils.constants as constants
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.data.connections.external.alluxio import alluxio_exception
from xpresso.ai.core.logging.xpr_log import XprLogger


class AlluxioConnector:
    """

    AlluxioConnector Class connects to the Filesystem through Alluxio

    :param alluxio_host_ip: Host IP for Alluxio Server (String object)
    :param alluxio_host_port: Host port for Alluxio Server (String object)

    """

    def __init__(self):
        """

        __init__() function initializes the host ip and port for Alluxio server.

        """

        self.logger = XprLogger()
        self.file = None
        with open(XprConfigParser().get_default_config_path()) as config_file:
            alluxio_config = json.load(config_file).get(constants.connectors)\
                .get(constants.alluxio)
        self.alluxio_host_ip = alluxio_config.get(constants.alluxio_ip)
        self.alluxio_host_port = alluxio_config.get(constants.alluxio_port)

    def alluxio_client(self):
        """

        Connects to the Alluxio Server

        :return: client: connector for client

        """

        try:
            client = alluxio.Client(self.alluxio_host_ip, int(self.alluxio_host_port))
            telnetlib.Telnet(self.alluxio_host_ip, int(self.alluxio_host_port))
            return client
        except alluxio.exceptions.AlluxioError as exc:
            self.logger.exception(exc)
            raise alluxio_exception.AlluxioClientException

    def file_byteformat_read(self, file_name):
        """

        Reads a file and keeps the data as bytes in an in-memory buffer

        :param file_name: Name of the required file

        :return: byte_format: stores file contents in byte-format

        """

        try:
            client = self.alluxio_client()
            with client.open(file_name, 'r') as self.file:
                byte_format = BytesIO(self.file.read())
            return byte_format
        except alluxio.exceptions.AlluxioError as exc:
            self.logger.exception(exc)
            raise alluxio_exception.AlluxioFileByteFormatReadException

    def save_multiple_files_raw(self, folder_path):
        """

        Saves multiple files from a folder in the File System on a local directory

        :param folder_path: Path of the folder from which the files are to be extracted
                            and saved on local filesystem (String object)

        :return: a pandas DataFrame containing information about the files

        """

        data_frame = pd.DataFrame(columns=[constants.FILE_NAME_COL,
                                           constants.FILE_SIZE_COl,
                                           constants.FILE_PATH_COL])
        client = self.alluxio_client()
        files = client.list_status(folder_path)
        for file in files:
            file_name = json.dumps(file.json().get("name")).replace('"', '')
            data_frame = data_frame.append(self.save_file_raw
                                           (os.path.join(folder_path, file_name)))
        return data_frame

    def save_file_raw(self, file_name):
        """

        Saves a file from the File System on a local directory

        :param file_name: Name of the file to be extracted
                         and saved on local filesystem (String object)

        :return: a pandas DataFrame containing information about the file

        """

        byte_format = self.file_byteformat_read(file_name)
        try:
            os.mkdir(os.path.join(os.getcwd(), constants.DOWNLOAD_DIR, ""))
        except FileExistsError:
            pass
        saved_file_path = os.path.join(constants.DOWNLOAD_DIR,
                                       os.path.basename(file_name))
        try:
            with open(saved_file_path, 'w') as self.file:
                self.file.write(str(byte_format.getvalue(), "UTF-8"))
        except UnicodeDecodeError:
            writer = pd.ExcelWriter(saved_file_path)
            pd.read_excel(byte_format).to_excel(writer, index=False)
            writer.save()
        data = pd.DataFrame([[os.path.basename(file_name),
                              str(os.stat(saved_file_path).st_size / constants.CONV_FACTOR),
                              saved_file_path]],
                            columns=[constants.FILE_NAME_COL,
                                     constants.FILE_SIZE_COl,
                                     constants.FILE_PATH_COL])
        data[constants.FILE_SIZE_COl] = data[constants.FILE_SIZE_COl].astype(float)
        return data

    def import_data(self, path, delimiter=constants.COMMA):
        """

        Imports a dataframe from the filesystem

        :param path: path/file_name of the file to be imported (String object)
        :param delimiter: separator used in the file

        :return: a pandas DataFrame

        """

        data_frame = None
        if delimiter is None:
            delimiter = constants.COMMA
        file_extension = os.path.splitext(path)
        if file_extension[1] == constants.text_extension:
            data_frame = self.get_data_text(path, delimiter)
        if file_extension[1] == constants.csv_extension:
            data_frame = self.get_data_csv(path, delimiter)
        if file_extension[1] == constants.excel_extension:
            data_frame = self.get_data_excel(path, delimiter)
        return data_frame

    def get_data_text(self, file_name, delimiter):
        """

        Gets data with .txt extension from the File System and
        converts it into Dataframe

        :param file_name: Name of the file to be extracted and
                        returned as dataframe (String object)
        :param delimiter: separator used in the file

        """

        byte_format = self.file_byteformat_read(file_name)
        data_frame = pd.read_csv(byte_format, sep=delimiter)
        return data_frame

    def get_data_csv(self, file_name, delimiter):
        """

        Gets data with .csv extension from the File System and
        converts it into Dataframe

        :param file_name: Name of the file to be extracted and
                         returned as dataframe. (String object)
        :param delimiter: separator used in the file

        """

        byte_format = self.file_byteformat_read(file_name)
        data_frame = pd.read_csv(byte_format, sep=delimiter)
        return data_frame

    def get_data_excel(self, file_name, delimiter):
        """

        Gets data with .xlsx extension from the File System and
        converts it into Dataframe

        :param file_name: Name of the file to be extracted and
                         returned as dataframe. (String object)
        :param delimiter: separator used in the file

        """
        byte_format = self.file_byteformat_read(file_name)
        data_frame = pd.read_excel(byte_format)
        return data_frame

    def close(self):
        """

        Method used to close all connections to the API

        :return: None

        """

        self.file.close()
