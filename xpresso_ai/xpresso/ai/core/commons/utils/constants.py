""" Stores constant variable """

# General constants to be used anywhere
LIBRARY_XPRESSO_PACKAGE_NAME = "xpresso_ai"
FLASK_JSONIFY_SORT_KEYS = 'JSON_SORT_KEYS'
PASSWORD_ENCRYPTION_KEY = b"U9fBoMYPoEl_oyAZOZD3_JVQMAQJ1Ax6uNRBhqT9NIc="
HTTP_OK = 200
permission_755 = 0o755
log_path_flag = 'log'
config_path_flag = 'config'
root_package_location = '/opt/xpresso.ai'
EMPTY_STRING = ""
CONFIG_DOCKER_REGISTRY = "docker_registry"
CONFIG_DOCKER_REGISTRY_HOST = "host"
CONFIG_DOCKER_REGISTRY_UID = "username"
CONFIG_DOCKER_REGISTRY_PWD = "password"
project_nfs_folder = "/mnt/nfs/data/projects"
database_component_type = "database"
component_deployment_manager_flavor = "component_deployment_manager_flavor"
secure_port_config = "secure_port"
MOUNT_PATH_KEY = 'mount_path'
CUSTOM_DOCKER_IMAGE_KEY = 'custom_docker_image'
BUILD_VERSION_KEY = 'build_version'
BUILD_DEPENDENCIES = "xpresso_dependencies"
BITBUCKET = 'bitbucket'
CURRENTLY_DEPLOYED_KEY = 'currentlyDeployed'
FLAVOR_KEY = 'flavor'
TYPE_KEY = 'type'
NAME_KEY = 'name'
COMPONENT_TYPE_SERVICE = 'service'
COMPONENT_TYPE_INFERENCE_SERVICE = 'inference_service'
COMPONENT_TYPE_JOB = 'job'
COMPONENT_TYPE_PIPELINE_JOB = 'pipeline_job'
CREATE_FLAG = 'create'
MODIFY_FLAG = 'modify'
PORTS_KEY = 'ports'
DESCRIPTION_KEY = 'description'
COMPONENTS_KEY = 'components'
PIPELINES_KEY = 'pipelines'
CONTROLLER_FIELD = "controller"
SERVER_URL_FIELD = "server_url"
UTF8_ENCODING = "utf-8"
ONE_GB_IN_BYTES = 1073741824
COMPONENT_FLAVOR_PYSPARK = "pyspark"

COMPONENT_NAME_KEY = "name"
COMPONENT_TYPE_KEY = "type"
LIBRARY_COMPONENT_KEY = "library"

CLIENT_PATH_FIELD = "client_path"
PY_EXTENSION = ".py"
ZIP_EXTENSION = ".zip"


# Project Management
REPO_NAME_PREFIX = "xpr"
REPO_NAME_SUFFIX = "sc"

# This is the environment variable that is used to identify xpresso directory
ENV_XPRESSO_PACKAGE_PATH = "XPRESSO_PACKAGE_PATH"
DEFAULT_XPRESSO_PACKAGE_PATH = root_package_location
ENV_XPRESSO_CONFIG_PATH = "XPRESSO_CONFIG_PATH"
DEFAULT_XPRESSO_CONFIG_PATH = "config/common.json"

# Pipeline constants
PIPELINE_FLAVOR = "flavor"
PIPELINE_FLAVOR_KUBEFLOW = 'kubeflow'
PIPELINE_FLAVOR_BIGDATA = 'pyspark'
PIPELINE_FLAVOR_ABSTRACT = 'abstract'
PIPELINE_FLAVOR_DEFAULT = PIPELINE_FLAVOR_KUBEFLOW
PROJECT_NAME_KEY = 'name'
PROJECT_COMPONENTS_KEY = 'components'
PROJECT_GIT_URL = "giturl"
variable_indicator = "##"
PROJECTS_SECTION = 'projects'
DECLARATIVE_PIPELINE_FOLDER = 'declarative_pipeline_folder'
KUBEFLOW_TEMPLATE = 'kubeflow_template'
SPARK_SUBMIT_FOLDER = 'spark_submit_folder'
SPARK_SUBMIT_TEMPLATE = 'spark_submit_template'
open_parenthesis = '('
single_quote = "'"
double_quote = '"'
escape_quote = '\\"'
test_deploy_id = 0
pipeline_conflict_error_code = 500
istio_gateway_service = 'istio-ingressgateway'
kubeflow_namespace = 'kubeflow'
istio_namespace = 'istio-system'
http2_port = 'http2'
kubeflow_installation_directory_name = 'kubeflow'
kubeflow_download_url = 'https://github.com/kubeflow/kubeflow/releases' \
                        '/download/v0.6.1/kfctl_v0.6.1_linux.tar.gz'
kubeflow_config_url = 'https://raw.githubusercontent.com/kubeflow/kubeflow/v0' \
                      '.6.1/bootstrap/config/kfctl_k8s_istio.yaml'
kubeflow_pvc_yaml_path = 'config/kubeflow-persistent-volumes.yaml'
RUN_PARAMETERS_KEY = 'run_parameters'
KUBEFLOW_CONFIG_KEY = "kubeflow"
MOUNT_LOCATION_PREFIX_CONFIG_KEY = "pv_mount_location_prefix"

# Declarative JSON constants
DECLARATIVE_JSON_TEMPLATE = {
        "name": "",
        "description": "",
        "components": [],
        "after_dependencies": {}
}
DECLARATIVE_JSON_COMPONENT_TEMPLATE = {
        "name": "",
        "implementation": {
          "container": {
            "command": [],
            "args": []
          }
        }
      }
PIPELINE_INPUT_STUB = \
    '{"name": "XPRESSO_INPUT_NAME", "type": "String", "description": ""}'
INPUT_VALUE_KEY = 'inputValue'
AFTER_DEPENDENCIES_KEY = 'after_dependencies'
INPUTS_KEY = 'inputs'
ARGS_KEY = 'args'
COMMAND_KEY = 'command'
CONTAINER_KEY = 'container'
IMPLEMENTATION_KEY = 'implementation'
IMAGE_KEY = 'image'
DECLARATIVE_JSON_KEY = 'declarative_json'
RUN_ID_PARAMETER = 'run_id'

# Kwargs used during deployment
pv_size_kwarg = "pv_size"
master_node_kwarg = "master_node"
pipeline_name_kwarg = "pipeline_name"
pipeline_versions_dict_kwarg = "pipeline_versions"
target_env_kwarg = "target_env"
mesh_name_kwarg = "mesh_name"
secure_port_kwarg = "secure_port"
parent_app_name_kwarg = "parent_app_name"
versions_list_kwarg = "versions_list"

# Local test repo for testing skeleton string replace
LOCAL_TEST_REPO = "skeleton-build"

# DB Setup
replset_initiate_cmd = "replSetInitiate"

# Bundle Input Json
bundle_local = "local"
bundle_cluster = "cluster"
xpresso_cluster_bundle = "XpressoClusterBundle"
xpresso_modular_cluster_bundle = 'XpressoModularClusterBundle'

# Kubernetes Constants
pv_size_cap = 10
kubernetes_GB_suffix = 'Gi'
type_pvc = "volume-claim"
type_pv = "volume"
pipeline_pv_prefix = "pipeline"
DEPLOYMENT_FILES_FOLDER = 'deployment_files_folder'
pipelines_storage_class = "pipelines"
deployments_storage_class = "deployments"
k8_conflict_error_code = 409
DEPLOYMENT_FLAVOR_KUBERNETES = "kubernetes"
k8_secure_port = '6443'
IS_EXTERNAL_KEY = 'is_external'
SVC_TYPE_NODEPORT = "NodePort"

# Spark Constants
DEPLOYMENT_FLAVOR_SPARK = "spark"

# xprctl
help_option_long = '--help'
help_option_short = '-h'
version_option_full = '--version'
OUTPUT_COLOR_GREEN = "green"
OUTPUT_COLOR_RED = "red"

# mongodb constants
port = '27017'
host = '172.16.3.1'
path = '/home/abzooba/Desktop/backup'
pipeline_versions_field = 'pipeline_versions'
pipeline_id_field = 'pipeline_id'
pipeline_name_field = 'name'
PIPELINE_COMPONENTS_FIELD = 'components'
pipeline_version_id_field = 'version_id'
PROJECT_NAME_IN_PIPELINES = "project_name"
PROJECT_NAME_IN_PROJECTS = "name"
PROJECT_TOKEN_IN_PROJECTS = "project_token"

# Mongo config DB
CONFIG_MONGO_SECTION = 'mongodb'
CONFIG_MONGO_URL = 'mongo_url'
CONFIG_MONGO_DB = 'database'
CONFIG_MONGO_UID = 'mongo_uid'
CONFIG_MONGO_PWD = 'mongo_pwd'
CONFIG_MONGO_W = 'w'
CONFIG_MONGO_EXPERIMENT = "experiment_collection"
CONFIG_MONGO_RUN = "run_collection"

# Mongo Collections
PIPELINE_COLLECTIONS = "pipelines"
PROJECT_COLLECTIONS = "projects"
USER_COLLECTIONS = "users"

# Confluence manual links
user_manual_link = "https://abzooba.atlassian.net/wiki/spaces/XS/" \
                   "pages/826081518/User+Management+Commands"
project_manual_link = "https://abzooba.atlassian.net/wiki/spaces/XS/" \
                      "pages/827424843/Project+Management+Commands"
follow_project_manual_message = "Please follow the project manual " \
                                f"{project_manual_link} and try again."
follow_user_manual_message = f"Please follow the user manual " \
                             f" {user_manual_link} and try again."

# data module constants
DEFAULT_OUTLIER_THRESHOLD_CATEGORICAL = 2
DEFAULT_OUTLIER_MARGIN_NUMERIC = 10
DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD = 95
DEFAULT_PROBABILITY_BINS = 15
DEFAULT_MODE_COUNT = 50
PERCENT_SIGN = "%"
SPACE_STRING = " "
LINE_SPACE = 10
DAY = 'D'
DEFAULT_SUBPLOT_COLUMNS = 2
UNIGRAM = 'grams_1'
BIGRAM = 'grams_2'
TRIGRAM = 'grams_3'
STOPWORD = 'stopword'
TEST_DATA_PATH = "./config/test/data/test.csv"
TEST_INVALID_DATA_PATH = "./config/test/data/test_invalid.csv"

# Unstructured Module constants
FILE_NAME_COL = "File Name"
FILE_PATH_COL = "Path"
FILE_SIZE_COl = "Size (in MB)"

# authentication constants
mongodb_auth_type = "mongodb"
ldap_auth_type = "ldap"

#  Data versioning constants
# user input variables
DV_INPUT_BRANCH_NAME = "branch_name"
DV_INPUT_COMMIT_ID = "commit_id"
DV_INPUT_DATASET_NAME = "dataset_name"
DV_INPUT_DESCRIPTION = "description"
DV_INPUT_REPO_NAME = "repo_name"
DV_INPUT_PATH = "path"
DV_INPUT_DATASET = "dataset"
DV_INPUT_UID = "uid"
DV_INPUT_PASSWORD = "pwd"
DV_INPUT_ENV = "env"
DV_PROJECT_TOKEN = "project_token"
DV_USER_TOKEN_FLAG = "user_token_flag"
DV_PROJECT_TOKEN_FLAG = "project_token_flag"

# Data Cleaning constants

# input dict key parameters
deduplicate_rows = "deduplicate"
considered_columns = "columns"
keep_row = "keep"
date = "date"
date_format = "format"
clean_dates = "clean_dates"

# Data connection constants
KEY_DATA_SOURCE = "data_source"
KEY_TYPE = "type"
KEY_PATH = "path"
VALUE_LOCAL = "local"

# input json key parameters
DSN = "DSN"
table = "table"
columns = "columns"
input_path = "path"
delimiter = "delimiter"
dataset_type = "dataset_type"
distributed = "distributed"

# BigQuery constants
cred_path = "cred_path"
project_id = 'project_id'
dataset = 'dataset'

# common.json parameters
connectors = "connectors"
presto = "presto"
presto_ip = "presto_ip"
presto_port = "presto_port"
presto_user = "presto_user"
catalog = "catalog"
schema = "schema"
alluxio = "alluxio"
alluxio_ip = "alluxio_ip"
alluxio_port = "alluxio_port"
hdfs = "hdfs"
hdfs_ip = "hdfs_ip"
hdfs_port = "hdfs_port"

# conversion factor for converting bytes to Megabytes
CONV_FACTOR = 1000 * 1000

# directory which will be created to download and save the raw files
DOWNLOAD_DIR = 'Downloaded_Files'

# path to data connector module
dataconnector_path = 'xpresso/ai/core/data/connector'

# the supported filetypes for data connector class
COMMA = ","
PIPE = "|"
DOT = '.'
text_extension = '.txt'
csv_extension = '.csv'
excel_extension = '.xlsx'
json_extension = ".json"

# data connector constants


USER_DB_CONSTANTS = {
    "UID": "uid",
    "PASSWORD": "pwd",
    "TOKEN": "token"
}

config_paths = {
    "default": "config/common.json",
    "dev": "config/common_dev.json",
    "prod": "config/common_prod.json",
    "qa": "config/common_stage.json",
    "sandbox": "config/common_sandbox.json"
}
DEFAULT_CONFIG_PATH_SETUP_LOG = "config/setup_docker.json"

# Visualization Command
PLOTLY_ORCA_PORT = 48000

# Exploration default paths and filenames
MAX_DATE_LENGTH = 26  # Date "Wednesday 26 February 2019"
MIN_DATE_LENGTH = 5  # Date "11/13"
N_GRAM_VALUE = 3
EXPLORER_OUTPUT_PATH = "./Exploration/"
EXPLORE_UNIVARIATE_FILENAME = "explore_univariate.xlsx"
EXPLORE_MULTIVARIATE_FILENAME = "explore_multivariate.xlsx"
REPORT_OUTPUT_PATH = "./Report/"
REPORT_UNIVARIATE_FILENAME = "report_univariate.pdf"
REPORT_MULTIVARIATE_FILENAME = "report_multivariate.pdf"

DEFAULT_GARBAGE_THRESHOLD = 5  # percent
PIE_CHART_OTHERS_THRESHOLD = 2  # percent for better visibility of categories
BAR_CHART_TAIL_THRESHOLD = 5  # percent for better visibility of significant
# bars

# Distributed EDA
TEMP_DIRECTORY = "./tmp/"
PARQUET_EXT = ".parquet"

# data serializtion constants
NGRAMS = ["unigram", "bigram", "trigram"]
CORRELATIONS = ["chi_square", "spearman", "pearson"]

# Controller Stub
STATUS = "status"
METRIC = "metric"
MESSAGE = "message"
OUTPUT_DIR = "/output/"
BRANCH_EXTENTION = "_model"
TIMEFORMAT = "%Y-%m-%d %H:%M:%S"
PKL_EXT = ".pkl"
DESCRIPTION = "Pushing output folder to pachyderm for model versioning"

# Access Roles
DEVELOPER = "dev"
PROJECT_MANAGER = "pm"
ADMIN = "admin"
SUPER_USER = "su"
PROJECT_OWNER = "owner"
NO_CHECK = "no_check"

# Project Constants
PIPELINES_PROJECT_COLLECTIONS = "pipelines"
VERSION_ID_PROJECT_COLLECTIONS = "deploy_version_id"
PIPELINE_FLAVOR_PROJECT_COLLECTIONS = "flavor"
PROJECT_OWNER_UID = "uid"
PROJECT_DEVELOPERS = "developers"

KEY_DOCKER_IMAGE = "dockerImage"
KEY_VERSIONS = "versions"

PROJECT_DISPLAY_KEY_RENAME_DICT = {
    "giturl": "Code Versioning Repo"
}

# Pipeline collection Constants
PROJECT_NAME_PIPELINE_COLLECTIONS = "project_name"
PIPELINES_PIPELINE_COLLECTIONS = "pipelines"
PIPELINE_NAME_PIPELINE_COLLECTIONS = "name"
PIPELINE_VERSIONS_PIPELINE_COLLECTIONS = "pipeline_versions"
VERSION_ID_PIPELINE_COLLECTIONS = "version_id"
PIPELINE_ID_PIPELINE_COLLECTIONS = "pipeline_id"
COMPONENTS_PIPELINE_COLLECTIONS = "components"

# XprResponse constants
OUTCOME_SUCCESS = "success"
OUTCOME_FAILURE = "failure"

# Data Versioning constants
VERSIONING_TOOL = "tool"
VERSIONING_CONFIG = "data_versioning"
VERSIONING_SERVER = "server"
VERSION_TOOL_HOST = "cluster_ip"
VERSION_TOOL_PORT = "port"
PACHYDERM_IN_CONFIG = "pachyderm"
OUTPUT_TYPE_FILES = "files"

# Experiment methods
CREATE_EXP = "create_experiment"
GET_EXP = "get_experiment_details"
START_EXP = "start_experiment"
CONTROL_EXP = "control_experiment"
PAUSE_EXP = "pause_experiment"
RESTART_EXP = "restart_experiment"
TERMINATE_EXP = "terminate_experiment"

# Abstract Pipeline Component
ENABLE_LOCAL_EXECUTION = "enable_local_execution"

BLANK = ""
TMP_DIR = "/tmp"

# Project Creation Steps
PROJECT_CREATED = "project_created"
REPO_CREATED = "repo_created"
NFS_FOLDER_CREATED = "nfs_directory_created"
DB_UPDATED = "db_updated"
ENV_ALLOCATED = "env_allocated_to_project"
BUILD_PIPELINE_CREATED = "created_build_pipeline"
PIPELINE_SETUP_COMPLETED = "deploy_pipelines-setup_complete"
PROJECT_UPDATED = "project_updated"
LOCAL_PROJECT_CREATED = "project_created_locally"
COMPONENT_DELETED = "component deleted successfully"
# User Input Constants for Project ops
INPUT_PROJECT_NAME = "name"
INPUT_PROJECT_DESCRIPTION = "projectDescription"
INPUT_PROJECT_COMPONENTS = "components"
INPUT_PROJECT_PIPELINES = "pipelines"
INPUT_NORMAL_ENVIRONMENT = "environments"
INPUT_BIGDATA_ENVIRONMENT = "bigdata_environments"
INPUT_PROJECT_COMPONENT_NAME = "name"

# BigData 
KEY_SPARK_CLUSTER = "spark_cluster"
KEY_SPARK_CLUSTER_MANAGER = "cluster_manager"
KEY_SERVICE_ACCOUNT = "service_account"
KEY_SPARK_K8S_SECRET = "secret"
KEY_SPARK_HOME = "spark_home"
SPARK_KEY_CLUSTER_ADMIN = "cluster_admin"
SPARK_KEY_MASTER = "master"
SPARK_KEY_JOB_UI_PORT = "job_ui_port"
SPARK_KEY_HOST = "host"
KEY_RUN_PARAMETERS = "run_parameters"
KEY_BRANCH = "branch"
KEY_COMMIT_ID = "commit_id"
SPARK_KEY_DEFAULT_RESOURCE_LIMITS = "default_resource_limits"
KEY_DRIVER_MEMORY = "driver_memory"
KEY_EXECUTOR_MEMORY = "executor_memory"
KEY_EXECUTOR_CORES = "executor_cores"
KEY_NUM_EXECUTOR = "num_executors"
KEY_JOB_NAME = "job_name"
KEY_RUN_NAME = "xpresso_run_name"
SPARK_K8S_CM = "k8s"
SPARK_YARN_CM = "yarn"
SPARK_KEY_IP = "ip"
SPARK_KEY_PORT = "port"
SPARK_K8S_SVC_ACCOUNT_NAME = "spark"
SPARK_K8S_ROLEBINDING_NAME = "spark-role"

# Service Mesh constants
SERVICE_MESH_KEY = 'service_mesh'
SERVICE_MESH_NAME_KEY = 'name'
SERVICE_MESH_COMPONENTS_KEY = 'components'
SERVICE_MESH_FLAVOR_ISTIO = 'istio'
SERVICE_MESH_STRATEGY_KEY = 'strategy'
SERVICE_MESH_VERSION_NAME_KEY = 'version_name'
ISTIO_API_VERSION = 'v1alpha3'
ISTIO_API_GROUP = 'networking.istio.io'
ISTIO_DESTINATION_RULE_PLURAL = 'destinationrules'
ISTIO_VIRTUAL_SERVICE_PLURAL = 'virtualservices'
ISTIO_WEIGHTS_STRATEGY_KEY = 'weights'
ISTIO_RULE_STRATEGY_KEY = 'rule'
ISTIO_HEADER_RULE_KEY = 'header'
HEADER_MATCH_TYPE_KEY = 'match_type'
HEADER_MATCH_STRING_KEY = 'match_string'
HEADER_VALUE_KEY = 'header_value'
ISTIO_DESTINATION_KEY = 'destination'
ISTIO_DEFAULT_DESTINATION_KEY = 'default_destination'
MATCH_TYPE_EXACT = 'exact'
MATCH_TYPE_REGEX = 'regex'
MATCH_TYPE_PREFIX = 'prefix'
istio_subsets_stub = '{"name": "ISTIO_VERSION_NAME", ' \
                     '"labels": {"version": "ISTIO_VERSION_NAME"}}'
istio_route_stub = '{"destination": {"host": "ISTIO_PARENT_APP_NAME", ' \
                   '"subset": "ISTIO_VERSION_NAME"},' \
                   '"weight": ISTIO_VERSION_WEIGHT}'
istio_match_header_stub = \
    '{"match": [{"headers": {"ISTIO_XPRESSO_HEADER_VALUE": ' \
    '{"ISTIO_XPRESSO_MATCH_TYPE": "ISTIO_XPRESSO_MATCH_STRING"}}}], ' \
    '"route": [{"destination": {"host": "ISTIO_XPRESSO_PARENT_APP",' \
    '"subset": "ISTIO_XPRESSO_DESTINATION"}}]}'
istio_header_default_route_stub = \
    '{"route": [{"destination": {"host": "ISTIO_XPRESSO_PARENT_APP", ' \
    '"subset": "ISTIO_XPRESSO_DEFAULT_DESTINATION"}}]}'
GRAFANA_DASHBOARD_SUFFIX_CONFIG = "dashboard_info_suffix"
GRAFANA_SVC_NAME = "grafana"
APP_LABEL_KEY = 'app'

# User DB Constants
PRIMARY_ROLE = "primaryRole"
UID = "uid"

# Inference Service constants
INFERENCE_SERVICE_KEY = 'inference_service'
RUNS_KEY = 'runs'
auto_mesh_creation_template_path = 'config/auto_mesh_creation_template'
MESH_NAME_KEY = 'mesh_name'
modify_project_json_stub = \
    '{"name" : "XPRESSO_PROJECT_NAME","service_mesh": [' \
    '{"name": "XPRESSO_INFERENCE_SERVICE_NAME",' \
    '"components": XPRESSO_INFERENCE_SERVICE_COMPONENTS}]}'
service_mesh_components_stub = \
    '{"version_name": "vXPRESSO_VERSION_NUMBER", "replicas": 1, ' \
    '"custom_docker_image": "XPRESSO_CUSTOM_DOCKER_IMAGE", ' \
    '"build_version": "XPRESSO_BUILD_VERSION", "environment_parameters": [' \
    '{"name": "REPO_NAME", "value": "XPRESSO_PROJECT_NAME"}, ' \
    '{"name": "COMMIT_ID", "value": "XPRESSO_COMMIT_ID"}, ' \
    '{"name": "DATA_VERSIONING_PATH","value": "XPRESSO_DV_PATH"}, ' \
    '{"name": "RUN_NAME","value": "XPRESSO_RUN_NAME"}],' \
    '"ports": XPRESSO_PORTS_ARRAY}'

# SSL Config
CONFIG_SSL = "ssl"
CONFIG_SSL_CERT_PATH = "cert_path"
CONFIG_SSL_PKEY_PATH = "pkey_path"
CONFIG_SSL_VERIFY = "verify"

# User related constants
USER_INPUT_UID = "uid"
UID_KEY_IN_DB = "uid"
USER_INPUT_PWD = "pwd"
PWD_KEY_IN_DB = "pwd"
TOKEN_KEY_IN_DB = "token"
SOFT_EXPIRY_KEY_IN_DB = "loginExpiry"
HARD_EXPIRY_KEY_IN_DB = "tokenExpiry"

# Miscellaneous
DOCKER_AUTH_STUB = '{"auths":{"XPRESSO_DOCKER_HOST/":{"username":' \
                   '"XPRESSO_DOCKER_UID","password":"XPRESSO_DOCKER_PWD",' \
                   '"auth":"XPRESSO_DOCKER_AUTH"}}}'

# Code Structure
CONFIG_STRUCTURE_KEY = "structure"
CONFIG_LIB_STRUCTURE_KEY = "library_structure_list"

# Token
TOKEN_EXPIRED = "token_expired"
LOGIN_EXPIRED = "login_expired"
NOT_EXPIRED = "not_expired"

# Dashboard
CONFIG_DASHBOARD = "dashboard"
CONFIG_DASHBOARD_BITBUCKET = "bitbucket"
CONFIG_DASHBOARD_KUBERNETES = "kubernetes"
CONFIG_DASHBOARD_JENKINS = "jenkins"
CONFIG_DASHBOARD_KIBANA = "kibana"
CONFIG_DASHBOARD_NFS = "nfs"
CONFIG_DASHBOARD_KUBEFLOW = "kubeflow"

CONFIG_BUNDLE_SETUP = "bundles_setup"
CONFIG_BUNDLE_SETUP_NFS = "nfs"
CONFIG_BUNDLE_SETUP_NFS_LOCATION = "nfs_location"

# User Context Constants
USER_CONTEXT = "user_context"
CONTEXT_USER_INFO_KEY = "user_info"
CONTEXT_PROJECT_LIST_KEY = "project_list"

DEFAULT_PASSWORD_VALUE = "GVVzuDa9knSTXMsdD7xhMxfXSaPurDApJffcdxPvLsqe2WsOvAOkTmBFIuXfYe"