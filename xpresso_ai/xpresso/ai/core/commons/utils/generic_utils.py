"""
This file contains generic utility functions
"""
import os
import socket
import re

from xpresso.ai.core.commons.utils.constants import ENV_XPRESSO_PACKAGE_PATH, \
    DEFAULT_XPRESSO_PACKAGE_PATH, \
    DEFAULT_XPRESSO_CONFIG_PATH, ENV_XPRESSO_CONFIG_PATH, \
    PASSWORD_ENCRYPTION_KEY


def get_base_pkg_location():
    """ Gets the xpresso base package location from the environment variable.
    If environment variable is not found, then return default value

    Returns:
        str: xpresso base package location
    """
    return os.environ.get(ENV_XPRESSO_PACKAGE_PATH,
                          DEFAULT_XPRESSO_PACKAGE_PATH)


def get_default_config_path():
    """ Gets the default config path location using environment variable.
    If no environment variable is present, then return default config

    Returns:
        str: config_path
    """
    return os.environ.get(ENV_XPRESSO_CONFIG_PATH,
                          DEFAULT_XPRESSO_CONFIG_PATH)


def get_version():
    """
    Fetches client and api_server versions and prints out in the stdout

    Returns:
        str: version string of the project
    """
    try:
        version_file_name = os.path.join(get_base_pkg_location(), 'VERSION')
        version_fs = open(version_file_name)
        version = version_fs.read().strip()
        version_fs.close()
    except FileNotFoundError:
        # Using default version
        version = '-1'
    return version


def check_if_valid_ip_address(ip_address):
    """ Checks if the ip address is valid """
    try:
        socket.inet_aton(ip_address)
        return True
    except socket.error:
        return False


def str2bool(string: str):
    """ Convert string to bool  value
    Args:
        string(str): string to convert to bool
    Returns:
        bool: True, if string is either true/True else False
    """
    if not string:
        return False
    true_set = {"true", "True"}
    return string.lower() in true_set


def encrypt_string(pwd: str) -> str:
    """ Encrypt string
    Args:
        pwd(str): string needed to be encrypted

    Returns:
        str: encrypted string
    """
    from cryptography.fernet import Fernet  # This is intentional

    cipher_suite = Fernet(PASSWORD_ENCRYPTION_KEY)
    encoded_text = cipher_suite.encrypt(pwd.encode("utf-8"))
    return encoded_text.decode("utf-8")


def decrypt_string(pwd: str) -> str:
    """ Decrypt string. Only those string which are envrypter using
    encrypt_string function can only be decrypted
    Args:
        pwd(str): string needed to be encrypted
    Returns:
        str: decrypted string
    """
    from cryptography.fernet import Fernet  # This is intentional

    cipher_suite = Fernet(PASSWORD_ENCRYPTION_KEY)
    encoded_text = cipher_suite.decrypt(pwd.encode("utf-8"))
    return encoded_text.decode("utf-8")


def convert_to_title(base_str: str) -> str:
    """ Convert the string into a title"""
    return re.sub("[^A-Za-z0-9]+", '', base_str.title())
