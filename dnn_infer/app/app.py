"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import os
import pandas as pd
import tensorflow as tf

from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger


__author__ = "Naveen Sinha"

logging = XprLogger("rossman_sales_project")


class DNNInference(AbstractInferenceService):

    def __init__(self):
        super().__init__()
        categorical_cols = ['Store', 'DayOfWeek', 'Promo', 'StateHoliday',
                            'SchoolHoliday', 'StoreType', 'Assortment',
                            'Promo2', 'Day', 'Month', 'Year', 'isCompetition']

        numeric_cols = ['CompetitionDistance']
        self.all_cols = categorical_cols + numeric_cols

    def exp_rmspe(self, y_true, y_pred):
        """Competition evaluation metric, expects logarthmic inputs."""
        pct = tf.square((tf.exp(y_true) - tf.exp(y_pred)) / tf.exp(y_true))
        # Compute mean excluding stores with zero denominator.
        x = tf.reduce_sum(tf.where(y_true > 0.001, pct, tf.zeros_like(pct)))
        y = tf.reduce_sum(
            tf.where(y_true > 0.001, tf.ones_like(pct), tf.zeros_like(pct)))
        return tf.sqrt(x / y)

    def act_sigmoid_scaled(self, x):
        """Sigmoid scaled to logarithm of maximum sales scaled by 20%."""
        return tf.nn.sigmoid(x) * tf.compat.v1.log(42000.00) * 1.2

    def load_model(self, model_path):
        CUSTOM_OBJECTS = {'exp_rmspe': self.exp_rmspe,
                          'act_sigmoid_scaled': self.act_sigmoid_scaled}
        self.model = tf.keras.models.load_model(os.path.join(model_path, "saved_model.h5"),
                                                custom_objects=CUSTOM_OBJECTS)

    def transform_input(self, input_request):
        data_to_list = [input_request[col] for col in self.all_cols]
        logging.info(data_to_list)
        feature = pd.DataFrame(data=[data_to_list],
                               columns=self.all_cols)
        logging.info([feature[col].values for col in self.all_cols])
        return feature

    def transform_output(self, output_response):
        return output_response

    def predict(self, input_request):
        tf_vector = [tf.convert_to_tensor(input_request[col].values) for col in
                     self.all_cols]
        value = self.model.predict(tf_vector)
        logging.info(value)
        logging.info(type(value[0]))
        return value[0].tolist()

    def report_inference_status(self, service_info, status):

        pass


if __name__ == "__main__":
    pred = DNNInference()
    # To run locally. Use load_model instead
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
