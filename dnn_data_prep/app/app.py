"""
This is the implementation of data preparation for sklearn
"""

import os
import sys

import pandas as pd
import numpy as np

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Naveen Sinha"

logging = XprLogger("rossman_sales_project")


class DNNDataPreparation(AbstractPipelineComponent):
    """ Core implementation of data preparation required for
      rosssman store sales
    """

    def __init__(self, train_file_path, test_file_path, store_file_path):
        super().__init__(name="DNN_DataPrep")
        self.train_data = pd.read_csv(train_file_path, parse_dates=['Date'])
        self.store_data = pd.read_csv(store_file_path)
        self.test_data = pd.read_csv(test_file_path, parse_dates=['Date'])
        self.combined_train_data = None
        self.combined_test_data = None

    @staticmethod
    def expand_date(train):

        train['Day'] = train['Date'].dt.day
        train['Week'] = train['Date'].dt.week
        train['Month'] = train['Date'].dt.month
        train['Year'] = train['Date'].dt.year

    @staticmethod
    def is_competition(row):
        if row['Year'] >= row['CompetitionOpenSinceYear'] and \
            row['Month'] >= row['CompetitionOpenSinceMonth']:
            return 1
        else:
            return 0

    @staticmethod
    def is_promo2(row):
        if row['Promo2'] == 1 and \
            row['Year'] >= row['Promo2SinceYear'] and \
            row['Week'] >= row['Promo2SinceWeek']:
            return 1
        else:
            return 0

    def start(self, run_name):
        super().start(xpresso_run_name=run_name)
        logging.info("Preparing data")
        # We need to correct some badly interpreted StateHoliday values 0 -> "0"
        self.train_data.loc[
            self.train_data['StateHoliday'] == 0, 'StateHoliday'] = "0"
        # Prior to do the join, we quickly modify the store dataframe to set 'Store' as index
        self.store_data.set_index('Store', inplace=True)
        logging.info("Combining train data...")
        combined_train_data = self.train_data.join(self.store_data, on='Store')
        combined_test_data = self.test_data.join(self.store_data, on='Store')
        self.send_metrics("Merged Sales and Customer Data", combined_train_data)
        combined_test_data.loc[np.isnan(combined_test_data['Open']), 'Open'] = 1
        combined_test_data['Open'] = combined_test_data['Open'].astype(int)
        logging.info("Updated open...")
        self.expand_date(train=combined_train_data)
        self.expand_date(train=combined_test_data)
        self.send_metrics("Expanded dates into year, month, week and day",
                          combined_train_data)
        # update is combined train
        combined_train_data['isCompetition'] = combined_train_data.apply(
            lambda row: self.is_competition(row),
            axis=1)
        combined_train_data.loc[
            np.isnan(combined_train_data[
                         'CompetitionDistance']), 'CompetitionDistance'] = 0
        self.expand_date(train=combined_test_data)
        logging.info("Adding competition distance...")
        # update is combined test
        combined_test_data['isCompetition'] = combined_test_data.apply(
            lambda row: self.is_competition(row),
            axis=1)
        combined_test_data.loc[
            np.isnan(combined_test_data[
                         'CompetitionDistance']), 'CompetitionDistance'] = 0
        logging.info("Adding promo...")
        combined_train_data['Promo2'] = combined_train_data.apply(
            lambda row: self.is_promo2(row), axis=1)
        combined_test_data['Promo2'] = combined_test_data.apply(
            lambda row: self.is_promo2(row), axis=1)
        self.send_metrics("Set promotion flag in data", combined_train_data)
        drop_columns = ['Date', 'Week',
                        'CompetitionOpenSinceMonth', 'CompetitionOpenSinceYear',
                        'Promo2SinceWeek', 'Promo2SinceYear', 'PromoInterval']
        combined_train_data.drop(drop_columns + ['Customers'], axis=1,
                                 inplace=True)
        combined_test_data.drop(drop_columns, axis=1, inplace=True)
        self.send_metrics("Dropped 8 irrelevant columns", combined_train_data)
        combined_train_data.drop(
            combined_train_data[combined_train_data['Open'] ==
                                0].index, inplace=True)
        combined_train_data.drop(['Open'], axis=1, inplace=True)
        combined_test_data.drop(combined_test_data[combined_test_data['Open'] ==
                                                   0].index, inplace=True)
        combined_test_data.drop(['Open'], axis=1, inplace=True)
        self.send_metrics("Dropped rows where Open = 0; Dropped Open column",
                          combined_train_data)
        combined_test_data.insert(loc=2, column='Sales', value=0)
        logging.info("Final update")
        self.send_metrics("Completed Data Preparation", combined_train_data)
        self.combined_train_data = combined_train_data
        self.combined_test_data = combined_test_data
        self.completed()

    def send_metrics(self, status, combined_data):
        report_status = {"status": {"status": status},
                         "rows": combined_data.shape[0],
                         "columns": combined_data.shape[1]
                         }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        output_dir = "/data/kaggle_dataset/"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        self.combined_train_data.to_csv(os.path.join(output_dir,
                                                     "dnn_combined_train.csv"),
                                        index=False)
        self.combined_test_data.to_csv(
            os.path.join(output_dir, "combined_test.csv"),
            index=False)
        logging.info("Data saved")
        super().completed(push_exp=False)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = DNNDataPreparation(
        train_file_path="/data/kaggle_dataset/train.csv",
        test_file_path="/data/kaggle_dataset/test.csv",
        store_file_path="/data/kaggle_dataset/store.csv")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
