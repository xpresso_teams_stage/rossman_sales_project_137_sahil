"""
This is the implementation of training for rossman store sales prediction
"""

__author__ = "Naveen Sinha"

import os
import sys
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.compat.v1.keras.backend as K
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Input, Embedding, Concatenate, Dense, \
    Flatten, Reshape, BatchNormalization, Dropout

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.INFO)

BATCH_SIZE = 128
TRAIN_EPOCHS = 10

logging = XprLogger("rossman_sales_project")


class LossAndErrorPrintingCallback(tf.keras.callbacks.Callback):

    def __init__(self, dnn_training_object):
        super().__init__()
        self.dnn_obj = dnn_training_object

    def on_epoch_end(self, epoch, logs=None):
        logging.info(
            'The average loss for epoch {} is {:7.2f}.'.format(epoch,
                                                               logs['loss']))
        logging.info(logs)
        report_status = {
            "status": {
                "status": "Epoch {}".format(epoch)
            },
           "loss": logs["loss"],
            "epoch": epoch
        }
        logging.info(report_status)
        self.dnn_obj.report_status(status=report_status)


class DNNTraining(AbstractPipelineComponent):
    def __init__(self, component_name, base_folder="/data/kaggle_dataset"):
        super().__init__(name=component_name)
        self.base_folder = base_folder
        self.split_train_path = os.path.join(self.base_folder,
                                             "split_train.csv")
        self.split_valid_path = os.path.join(self.base_folder,
                                             "split_valid.csv")
        self.orig_train = None

        self.train = None
        self.valid = None
        self.feature_names = None
        self.csv_defaults = None

    def load(self):
        self.orig_train = pd.read_csv(
            os.path.join(self.base_folder,
                         "dnn_combined_train.csv"))
        self.orig_train = self.orig_train.sample(frac=0.01)

    def build_vocabulary(self, df: pd.DataFrame, cols):
        vocab = {}
        for col in cols:
            values = df[col].unique()
            col_type = type([x for x in values if x is not None][0])
            default_value = col_type()
            vocab[col] = sorted(values, key=lambda x: x or default_value)
        return vocab

    def cast_columns(self, df: pd.DataFrame, cols):
        for col in cols:
            df[col] = df[col].astype(float)
        return df

    def lookup_columns(self, df, vocab):
        for col, mapping in vocab.items():
            df[col] = df[col].apply(mapping.index)
            df[col] = df[col].astype(float)
        return df

    def exp_rmspe(self, y_true, y_pred):
        """Competition evaluation metric, expects logarthmic inputs."""
        pct = tf.square((tf.exp(y_true) - tf.exp(y_pred)) / tf.exp(y_true))
        # Compute mean excluding stores with zero denominator.
        x = tf.reduce_sum(tf.where(y_true > 0.0000001, pct, tf.zeros_like(pct)))
        y = tf.reduce_sum(
            tf.where(y_true > 0.0000001, tf.ones_like(pct), tf.zeros_like(pct)))
        return tf.sqrt(y_true / y_pred)

    def act_sigmoid_scaled(self, x):
        """Sigmoid scaled to logarithm of maximum sales scaled by 20%."""
        return tf.nn.sigmoid(x) * tf.compat.v1.log(42000.00) * 1.2

    def start(self, run_name):
        super().start(xpresso_run_name=run_name)
        logging.info("preparing tensor")
        self.report_status(status={"status": {"status": "Preparing tensor"}})
        categorical_cols = ['Store', 'DayOfWeek', 'Promo', 'StateHoliday',
                            'SchoolHoliday', 'StoreType', 'Assortment',
                            'Promo2', 'Day', 'Month', 'Year', 'isCompetition']

        numeric_cols = ['CompetitionDistance']

        all_cols = categorical_cols + numeric_cols
        CUSTOM_OBJECTS = {'exp_rmspe': self.exp_rmspe,
                          'act_sigmoid_scaled': self.act_sigmoid_scaled}
        self.report_status(status={"status": {"status": "Building vocabulary"}})
        self.max_sales = float(self.orig_train['Sales'].max())
        vocab = self.build_vocabulary(self.orig_train[categorical_cols],
                                      categorical_cols)
        self.orig_train = self.cast_columns(self.orig_train,
                                            numeric_cols + ['Sales'])
        self.orig_train = self.lookup_columns(self.orig_train, vocab)
        self.report_status(status={"status": {"status": "Starting session "}})
        train, valid = train_test_split(self.orig_train, test_size=0.05,
                                        random_state=42)
        logging.info(train.head())
        logging.info(valid.head())
        logging.info("Starting session")
        config = tf.compat.v1.ConfigProto(device_count={'GPU': 0})
        K.set_session(tf.compat.v1.Session(config=config))
        self.report_status(status={"status": {"status": "Building model"}})
        # Build the model.
        logging.info("Building model")
        inputs = {col: Input(shape=(1,), name=col) for col in all_cols}
        embeddings = [
            Embedding(len(vocab[col]), 10, input_length=1, name='emb_' + col)(
                inputs[col])
            for col in categorical_cols]
        continuous_bn = Reshape((1, 1), name='reshape_' + numeric_cols[0])(
            inputs[numeric_cols[0]])
        continuous_bn = BatchNormalization()(continuous_bn)
        x = Concatenate()(embeddings + [continuous_bn])
        x = Flatten()(x)
        x = Dense(1000, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dense(1000, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dense(1000, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dense(500, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dropout(0.5)(x)
        output = Dense(1, activation=self.act_sigmoid_scaled)(x)
        model = tf.keras.Model([inputs[f] for f in all_cols], output)
        model.summary()
        opt = tf.keras.optimizers.Adam(lr=1e-4, epsilon=1e-3)
        model.compile(opt, 'mae', metrics=[self.exp_rmspe, "accuracy"])
        self.report_status(status={"status": {"status": "Training model now"}})
        logging.info("Training model")
        history = model.fit(
            x=[tf.convert_to_tensor(train[col].values) for col in all_cols],
            y=[tf.convert_to_tensor(train['Sales'].values)],
            callbacks=[LossAndErrorPrintingCallback(self)],
            verbose=2,
            batch_size=100,
            validation_steps=len(valid) / BATCH_SIZE,
            steps_per_epoch=len(train) / BATCH_SIZE,
            epochs=1,
            validation_data=
            ([tf.convert_to_tensor(valid[col].values) for col in all_cols],
             [tf.convert_to_tensor(valid['Sales'].values)]))
        self.report_status(status={"status": {"status": "Model trained. Saving"},
                                    "exp_rmspe": history.history.get('exp_rmspe')[-1],
                                    "loss": history.history.get('loss')[-1]
                                   })
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        model.save(os.path.join(self.OUTPUT_DIR, 'saved_model.h5'))
        logging.info("Saved model... load model")
        self.report_status(status={"status": {"status": "Training completed"}})
        super().completed(push_exp=True)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    trainer = DNNTraining(component_name="DNN_Train",
                          base_folder="/data/kaggle_dataset/")
    trainer.load()
    if len(sys.argv) >= 2:
        trainer.start(run_name=sys.argv[1])
    else:
        trainer.start(run_name="")
